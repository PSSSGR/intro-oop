/*
 * Filename:	program1.cpp 
 * Name:		Sri Padala
 * Description:	This program builds up the account profile of a bank account and make transactoins as directed. 
 */

#include<iostream>
#include<cmath>

using namespace std;

struct Money{//Money values are stored in the struct.
	int dollar;
	int cent;
};
struct Account{//Account information is stored in the struct.
	Money money;
	double interest_rate;
	string account_name;
};

Account createAccount();//Creates profile of the user.
Account Deposit(Account account, Money deposit);//Makes deposits.
Money Withdraw(Account &account, Money withdraw);//Makes withdraws.
void accrue(Account &account);
void print(Account account);
void print(Money account);

int main (void){
	Account Savings;//
	Account Checking;//Used for the transactions associated with the operations.
	Account Roth_IRA;//
	Money deposit;//Represents deposit amount.
	Money withdraw;//Represents withdraw amount.
	Money withdraw2;//Represents withdraw amount.
	Money error;//Represents the negative amount. 

	Savings.money.dollar=50;//Initializing the values of the savings struct .
	Savings.money.cent=0;
	Savings.account_name="Savings";
	Checking.money.dollar=10;//Initializing the values of the Checking struct .
	Checking.money.cent=0;
	Checking.account_name="Checking";
	Roth_IRA.account_name="Roth IRA";
	deposit.dollar=50;//Initializing the values of the deposit struct .
	deposit.cent=0;
	withdraw.dollar=14;//Initializing the values of the withdraw struct .
	withdraw.cent=99;
	withdraw2.dollar=200;//Initializing the values of the withdraw2 struct .
	withdraw2.cent=0;
	error.dollar=-50;//Initializing the values of the error struct .
	error.cent=0;//Makes some negative transactions.

	cout<<"In "<<Savings.account_name<<", you have ";
	print(Savings.money);//prints Savings amount.
	cout<<".";

	cout<<"\nIn "<<Checking.account_name<<", you have ";
	print(Checking.money);//prints Checkings amount.
	cout<<".";

	Roth_IRA=createAccount();
	cout<<"\nIn your "<<Roth_IRA.account_name<<", you have ";
	print(Roth_IRA.money);//prints account amount.
	cout<<"."<<endl<<endl;

	Savings=Deposit(Savings ,deposit);
	cout<<"You now have ";
	print(Savings.money);
	cout<<" in your "<<Savings.account_name <<" account."<<endl<<endl;


	withdraw=Withdraw(Checking,withdraw);
	cout<<"Withdrew ";
	print(withdraw);
	cout<<" for HBO Now, you now have ";
	print(Checking);
	cout<<endl<<endl;

	for(int i=0;i<2;i++){//Makes two increments to account money.
	accrue(Roth_IRA);
	cout<<"You now have ";
	print(Roth_IRA);
	cout<<" in your "<<Roth_IRA.account_name<<"."<<endl;
	}
	cout<<"\nAn unexpected expense appears!"<<endl;
	print(withdraw2);
	cout<<"\nAttempting to withdraw from "<<Savings.account_name<<"."<<endl;;
	withdraw2=Withdraw(Savings,withdraw2);//withdrawing more amount.
	cout<<"It’s not very effective."<<endl;
	cout<<"You now have ";
	print(Savings);
	cout<<" in your "<<Savings.account_name<<" account."<<endl;

	cout<<"\nAttempting some bad withdraws and deposits."<<endl;//Make Transactions with negative amounts.
	Roth_IRA=Deposit(Roth_IRA,error);
	Withdraw(Roth_IRA,error);




	return(0);
}


Account createAccount(){
	Account temp;
	double balance;
	cout<<"\n\nLet’s set up your account."<<endl;
	cout<<"First, what’s the name of the account?";
	getline(cin,temp.account_name);
	cout<<"\nWhat is the interest rate of your "<<temp.account_name<<" account?";
	cin>>temp.interest_rate;
	cout<<"\nFinally, what is the starting balance of your "<<temp.account_name<<" account? $";
	cin>>balance;
	temp.money.dollar=trunc(balance);//gives the dollar amount.
	temp.money.cent=round((balance-temp.money.dollar)*100);//gives the cent amount.

	return(temp);
}

Account Deposit(Account account, Money deposit){

	int deposit_amount=((deposit.dollar)*100)+deposit.cent;//Converts money struct to money amount.
	if(deposit_amount>=0){
	account.money.dollar+=deposit.dollar;
	account.money.cent+=deposit.cent;
	print(deposit);
	cout<<" deposited into "<<account.account_name<<endl; 
	}else{
		cout<<"Cannot make negative deposit. "<<endl;
	}

	return(account);
}

Money Withdraw(Account &account, Money withdraw){
	int withdraw_amount=((withdraw.dollar)*100)+withdraw.cent;//converts withdraw struct to money amount.
	double amount= static_cast<double>(withdraw_amount/100);
	int account_amount=((account.money.dollar)*100)+account.money.cent;//converts accoutn struct to money amount.
	double amount_account=static_cast<double>(account_amount/100);
	if(withdraw_amount>=0){
		if(amount-amount_account<=50){//Checks the condition to maximum withdrawal.
			account.money.dollar-=withdraw.dollar;
			account.money.cent-=withdraw.cent;
			print(withdraw);
			cout<<" withdrawn from "<< account.account_name <<"."<<endl;
		}else{
			amount=amount_account+50;
			withdraw.dollar=trunc(amount);
			withdraw.cent=(amount-withdraw.dollar)*100;
			account.money.dollar-=withdraw.dollar;
			account.money.cent-=withdraw.cent;
			print(withdraw);
			cout<<" withdrawn from "<< account.account_name <<"."<<endl;
		}
	}else{//if a negative amount is withdrawed.
		cout<<"Cannot make negative withdrawl."<<endl;
		withdraw.dollar=0;
		withdraw.cent=0;
		print(withdraw);
		cout<<" withdrawn from "<<account.account_name<<"."<<endl;
	}
	return(withdraw);
}

void accrue(Account &account){
	Account temp;
	Money temp1;
	double amount,temp_amount,final_amount;
	temp.money.dollar=(account.money.dollar)*100;//Converting money struct to money value.
	temp.money.cent=account.money.cent;
	amount=(temp.money.dollar)+(temp.money.cent);
	temp_amount=static_cast<double>((amount)*(account.interest_rate));
	final_amount=amount+temp_amount;
	final_amount=static_cast<double>(final_amount/100);
	account.money.dollar=trunc(final_amount);
	account.money.cent=round(((final_amount)-(account.money.dollar))*100);//Takes care of the errors.

	cout.setf(ios::fixed);//sets presision for two decimal points.
	cout.setf(ios::showpoint);
	cout.precision(2);
	double temp2=static_cast<double>(temp_amount/100);
	temp1.dollar=trunc(temp2);
	temp1.cent=round((temp2-temp1.dollar)*100);

	cout<<"At "<<(account.interest_rate)*100<<"%, Your "<<account.account_name<<" account earned ";
	print(temp1);
	cout<<"."<<endl;
}

void print(Account account){
	double amount;
	amount=((account.money.dollar)*100)+account.money.cent;
	if(amount>=0){
		cout<<"$";
		amount=static_cast<double>(amount/100);
	   	cout.setf(ios::fixed);
		cout.setf(ios::showpoint);
		cout.precision(2);
		cout<<amount;
	}else{//prints negative values.

		account.money.dollar=abs(account.money.dollar);
		account.money.cent=abs(account.money.cent);
		amount=((account.money.dollar)*100)+account.money.cent;
		amount=static_cast<double>(amount/100);
		cout.setf(ios::fixed);
		cout.setf(ios::showpoint);
		cout.precision(2);
		cout<<"($";
		cout<<amount;
		cout<<")";
	}
}

void print(Money account){
	double amount;
	amount=((account.dollar)*100)+account.cent;
	if(amount>=0){
		amount=static_cast<double>(amount/100);
		cout.setf(ios::fixed);
		cout.setf(ios::showpoint);
		cout.precision(2);
		cout<<"$";
		cout<<amount;
	}else{//prints negative values.
		account.dollar=abs(account.dollar);
		account.cent=abs(account.cent);
		amount=static_cast<double>(amount/100);
		cout.setf(ios::fixed);
		cout.setf(ios::showpoint);
		cout.precision(2);
		cout<<"($";
		cout<<amount;
		cout<<")";
	}
}

	
































