/*
 * Filename:	program2.cpp 
 * Name:		Sri Padala
 * Description:	This program builds up the account profile of a bank account and make transactoins as directed. 
 */
// Add Or Subtract Libraries As Needed
#include <cmath>
#include <iomanip>
#include <iostream>
#include <string>
#include <cmath>
#include <cstdlib>

// Place Money Class Definition Here
class Money{
	public:
		Money();//Constructors
		Money(int dollars,int cents);
		Money(int amount);
		Money(double amount);
		int getPennies() const;
		bool isNegative() const;
		void add(const Money &m);
		void subtract(const Money &m);
		bool isEqual(const Money &m) const;
		void print() const ;
	private:
		int dollars;
		int cents;
};


// Place Account Class Definition Here
class Account{
	public:
		Account();//Constructors
		Account(std::string account_name, double Interest_rate, Money balance);
		Account(std::string account_name, double Interest_rate, int balance);
		Account(std::string account_name, double Interest_rate, double balance);
		std::string getName() const;
		double getRate() const;
		const Money getBalance() const;
		void setName(std::string newName); 
		void deposit(const Money &m);
		void deposit(int d, int c);
		void deposit(int d);
		void deposit(double d); 
		const Money withdraw(const Money &m);
		const Money withdraw(int d, int c);
		const Money withdraw(int d);
		const Money withdraw(double d);
		void accrue(); 
		void print() const; 
		void transferTo(Account &dest, const Money &amount);
	private:
		std::string Account_name;
		double Interest_rate;
		Money money;

};


// Define Non-Member Function Here
const Account createAccount(); 


// Test Functions; *** DO NOT ALTER ***
bool moneyGetPennies(int arg);
bool moneyIsNegative(bool expected);
bool moneyAdd();
bool moneySubtractPos();
bool moneySubtractNeg();
void moneyPrint(double arg);
bool acctGetName();
bool acctGetRate();
bool acctGetBalance();
bool acctSetName();
bool acctDep01();
bool acctDep02();
bool acctDep03();
bool acctDep04();
void acctBadDep();
bool acctWith01();
bool acctWith02();
bool acctWith03();
bool acctWith04();
bool acctWithPart01();
bool acctWithPart02();
void acctBadWith();
bool acctAccrue();
void acctPrint(double arg);
bool acctTransfer();

// Main Function; *** DO NOT ALTER ***
int main()
{
	char prev = std::cout.fill('.');

	std::cout << "*** TESTING MONEY CLASS FUNCTIONS ***\n";
	std::cout << std::setw(57) << std::left << "1. Testing Money class GetPennies:" << (moneyGetPennies(555)) << std::endl;
	std::cout << std::setw(57) << std::left << "2. Testing Money class GetPennies:" << (moneyGetPennies(-555)) << std::endl;
	std::cout << std::setw(57) << std::left << "3. Testing Money class IsNegative:" << (moneyIsNegative(true)) << std::endl;
	std::cout << std::setw(57) << std::left << "4. Testing Money class IsNegative:" << (moneyIsNegative(false)) << std::endl;
	std::cout << std::setw(57) << std::left << "5. Testing Money class Add:" << (moneyAdd()) << std::endl;
	std::cout << std::setw(57) << std::left << "6. Testing Money class Subtract:" <<  (moneySubtractPos()) << std::endl;
	std::cout << std::setw(57) << std::left << "7. Testing Money class Subtract:" <<  (moneySubtractNeg()) << std::endl;
	std::cout << std::setw(57) << std::left << "8. Testing Money class Print ( Must match $5.44 ):"; (moneyPrint(5.44)); std::cout << std::endl;
	std::cout << std::setw(57) << std::left << "9. Testing Money class Print ( Must match ($5.44) ):"; (moneyPrint(-5.44)); std::cout << std::endl;

	std::cout << "\n*** TESTING ACCOUNT CLASS FUNCTIONS ***\n";
	std::cout << std::setw(4) << std::left << std::setfill(' ') << "1. " << std::setw(53) << std::left << std::setfill('.') << "Testing Account class GetName:" << (acctGetName()) << std::endl;
	std::cout << std::setw(4) << std::left << std::setfill(' ') << "2. " << std::setw(53) << std::left << std::setfill('.') << "Testing Account class GetRate:" << (acctGetRate()) << std::endl;
	std::cout << std::setw(4) << std::left << std::setfill(' ') << "3. " << std::setw(53) << std::left << std::setfill('.') << "Testing Account class GetBalance:" << (acctGetBalance()) << std::endl;
	std::cout << std::setw(4) << std::left << std::setfill(' ') << "4. " << std::setw(53) << std::left << std::setfill('.') << "Testing Account class SetName:" << (acctSetName()) << std::endl;
	std::cout << std::setw(4) << std::left << std::setfill(' ') << "5. " << std::setw(53) << std::left << std::setfill('.') << "Testing Account class Deposit #1:" << (acctDep01()) << std::endl;
	std::cout << std::setw(4) << std::left << std::setfill(' ') << "6. " << std::setw(53) << std::left << std::setfill('.') << "Testing Account class Deposit #2:" << (acctDep02()) << std::endl;
	std::cout << std::setw(4) << std::left << std::setfill(' ') << "7. " << std::setw(53) << std::left << std::setfill('.') << "Testing Account class Deposit #3:" << (acctDep03()) << std::endl;
	std::cout << std::setw(4) << std::left << std::setfill(' ') << "8. " << std::setw(53) << std::left << std::setfill('.') << "Testing Account class Deposit #4:" << (acctDep04()) << std::endl;
	std::cout << std::setw(4) << std::left << std::setfill(' ') << "9. " << std::setw(53) << std::left << std::setfill('.') << "Testing Account class Deposit (ERROR):"; acctBadDep();std::cout<<std::endl; 
	std::cout << std::setw(4) << std::left << "10. " << std::setw(53) << std::left << "Testing Account class Withdraw #1:" << (acctWith01()) << std::endl;
	std::cout << std::setw(4) << std::left << "11. " << std::setw(53) << std::left << "Testing Account class Withdraw #2:" << (acctWith02()) << std::endl;
	std::cout << std::setw(4) << std::left << "12. " << std::setw(53) << std::left << "Testing Account class Withdraw #3:" << (acctWith03()) << std::endl;
	std::cout << std::setw(4) << std::left << "13. " << std::setw(53) << std::left << "Testing Account class Withdraw #4:" << (acctWith04()) << std::endl;
	std::cout << std::setw(4) << std::left << "14. " << std::setw(53) << std::left << "Testing Account class Withdraw (OVERDRAW):" << (acctWithPart01()) << std::endl;
	std::cout << std::setw(4) << std::left << "15. " << std::setw(53) << std::left << "Testing Account class Withdraw (PARTIAL):" << (acctWithPart02()) << std::endl;
	std::cout << std::setw(4) << std::left << "16. " << std::setw(53) << std::left << "Testing Account class Withdraw (ERROR):"; acctBadWith();std::cout<<std::endl;
	std::cout << std::setw(4) << std::left << "17. " << std::setw(53) << std::left << "Testing Account class Accrue: " << (acctAccrue()) << std::endl;
	std::cout << std::setw(4) << std::left << "18. " << std::setw(53) << std::left << "Testing Account class Transfer: " << (acctTransfer()) << std::endl;
	std::cout << std::setw(4) << std::left << "19. " << std::setw(53) << std::left << "Testing Account class Print ( Must match $50.00 ):"; (acctPrint(50.00)); std::cout << std::endl;
	std::cout << std::setw(4) << std::left << "20. " << std::setw(53) << std::left << "Testing Account class Print ( Must match ($50.00) ):"; (acctPrint(-50.00)); std::cout << std::endl;

	Account account = createAccount();
	std::cout << "\n\n*** TESTING NON-CLASS FUNCTION CREATEACCOUNT ***\n";
	std::cout << std::setw(19) << std::left << "1. Account Name:" << account.getName() << std::endl;
	std::cout << std::setw(19) << std::left << "2. Interest Rate:" << account.getRate() << std::endl; 
	std::cout << std::setw(19) << std::left << "3. Balance:"; (account.getBalance()).print(); std::cout << std::endl; 

	std::cout.fill(prev);

	return 0;
}
 // Implement Money Class Functions Here
Money::Money() // A default constructor that initializes your Money object to $0.00.
	:dollars(0), cents(0) {
}

Money::Money(int dollars, int cents) //A constructor that takes two integers, the ﬁrst for the dollars and the second for the cents .
	:dollars(dollars), cents(cents) {
}

Money::Money(int dollars)// A constructor that takes one integer, for an clean dollar amount .
	:dollars(dollars), cents(0){
}

Money::Money(double amount)//A constructor that takes one double, and can assign dollars and cents accordingly .
	: dollars(trunc(amount)), cents((round((amount-trunc(amount))*100))){
}
 
int Money::getPennies() const {//Returns the equivalent amount of money as a number of pennies 
	int totalPennies = (dollars) * 100 + (cents);
	return totalPennies;
}
 
bool Money::isNegative() const {
	if (dollars < 0 || cents < 0)//Checks if your amount of money is negative .
		return true;
	else
		return false;
}
 
void Money::add(const Money &m) {// The sum shall be stored in the calling object 
	int sum;
	sum = getPennies() + m.getPennies();
	int tempD, tempC;
	tempD = (sum) / 100;//Dollar part
	tempC = (sum) % 100;//Cent part 
	dollars = tempD;
	cents = tempC;
}
 
void Money::subtract(const Money &m) {
	int diff;
	diff = getPennies() - m.getPennies();
	int tempD, tempC;
	tempD = (diff) / 100;//Dollar part
	tempC = (diff) % 100;//Cent part
	dollars = tempD;
	cents = tempC;
}
 
bool Money::isEqual(const Money &m) const {
	if ((dollars == m.dollars) && (cents == m.cents)){
		return true;
	}else{
		return false;
	}
}
 
void Money::print() const {
	if (isNegative() == true) {//Print the negative amount.
		std::cout << "($" << std::abs(dollars) << ".";
		if ((cents < 10 && cents >= 0) || (cents > -10 && cents <= 0)){
			std::cout << "0";
		}
		std::cout << std::abs(cents) << ")";
	}else{
		std::cout << "$" << dollars << ".";
		if (cents < 10){
			std::cout << "0";
		}
		std::cout << cents;
	}
}



// Implement Account Class Functions Here
Account::Account(): Account_name("Savings"), Interest_rate(0), money(0,0) {};
Account::Account(std::string account_name, double Interest_rate, Money balance) : Account_name(account_name), Interest_rate(Interest_rate), money(balance) {};
Account::Account(std::string account_name, double Interest_rate, int balance) : Account_name(account_name), Interest_rate(Interest_rate), money(balance) {};
Account::Account(std::string account_name, double Interest_rate, double balance) : Account_name(account_name), Interest_rate(Interest_rate),money(trunc(balance),round((balance-(trunc(balance)))*100)) {
}
 
std::string Account::getName() const {
	return Account_name;
}
 
double Account::getRate() const {
	return Interest_rate;
}
 
const Money Account::getBalance() const {
	Money a;
	a = money;
	return a;
}
 
void Account::setName(std::string newName) {
	Account_name=newName;
}
 
void Account::deposit(const Money &m) {
 
	if (m.isNegative() == 1) {
		std::cout << "Cannot deposit a negative amount.";
	}else {
		money.add(m);
	}
}
 
void Account::deposit(int d, int c) {
	Money temp(d, c);
	if (temp.isNegative() == 1) {
		std::cout << "Cannot deposit a negative amount.";
	}else {
		money.add(temp);
	}
 
}
 
void Account::deposit(int d) {
	Money temp(d);
	if (temp.isNegative() == 1) {
		std::cout << "Cannot deposit a negative amount.";
	}else {
		money.add(temp);
	}
}
 
void Account::deposit(double d) {
	Money temp(d);
	if (temp.isNegative() == 1) {
		std::cout << "Cannot deposit a negative amount.";
	}else {
		money.add(temp);
	}
}
const Money Account::withdraw(const Money &m){
	int temp = money.getPennies();
	int temp2 = m.getPennies();
	
	if (m.isNegative() == 1) {
		std::cout << "Cannot withdraw a negative amount.";
		return(m);
	}else{
		if(temp-temp2 >= -5000){//Checks to not overdraw more than $50. 
			 money.subtract(m);
			 return(m);
		}else{
			Money balance;
			temp=temp+5000;
			balance=Money(temp);
			money.subtract(balance);
			return(balance);
		}
	}

}
const Money Account::withdraw(int d, int c){
	Money temp(d, c);
	int temp1 = money.getPennies();
	int temp2 = temp.getPennies();

	if (temp.isNegative() == 1) {
		std::cout << "Cannot withdraw a negative amount.";
		return(temp);
	}else{
		if (temp1-temp2 >= -5000) {//Checks to not overdraw more than $50. 
			money.subtract(temp);
			return(temp);
		}else{
			Money balance;
			temp1=temp1+5000;
			balance=Money(temp1);
			money.subtract(balance);
			return(balance);
		}

	}
}

const Money Account::withdraw(int d){
	Money temp(d);
	int temp1 = money.getPennies();
	int temp2 = temp.getPennies();
	if (temp.isNegative() == 1) {
		std::cout << "Cannot withdraw a negative amount.";
		return(temp);
	}else{
		if (temp1-temp2 >= -5000) {//Checks to not overdraw more than $50. 
			money.subtract(temp);
			return(temp);
		}else{
			Money balance;
			temp1=temp1+5000;
			int a =(temp1/100);
			int b =(temp1%100);
			balance=Money(a,b);
			money.subtract(balance);
			return(balance);
		}

	}
}
const Money Account::withdraw(double d){
	Money temp(d);
	int temp1 = money.getPennies();
	int temp2 = temp.getPennies();
	if (temp.isNegative() == 1) {
		std::cout << "Cannot withdraw a negative amount.";
		return(temp);
	}else{
		if (temp1-temp2 >= -5000) {//Checks to not overdraw more than $50. 
			money.subtract(temp);
			return(temp);
		}else{
			Money balance;
			temp1=temp1+5000;
			balance=Money(temp1);
			money.subtract(balance);
			return(balance);
		}

	}

}
 
void Account::accrue() {
	int startBal, endBal;
 
	startBal = money.getPennies();
	endBal = startBal + (startBal * Interest_rate);
	
	int tempD, tempC;
	tempD = (endBal) / 100;//Dollar part
	tempC = (endBal) % 100;//cent part
	money = Money ( tempD, tempC);
		
}
 

 
void Account::print() const {
	money.print();
}
 
void Account::transferTo(Account &dest, const Money &amount) {
	if (amount.isNegative() == 1)
		std::cout << "You cannot transfer a negative amount of Money." << std::endl;
	else {
		money.subtract(amount);
		dest.money.add(amount);
	}
}



// Implement Non-Member Function Here
const Account createAccount() {
	double amt, rate;
	std::string name,amount;
 
 
 
	std::cout << "\nLet's set up your account. \nFirst, what's the name of the account?";
	getline(std::cin, name);
	std::cout << "\nWhat is the interest rate of your " << name << " account?";
	std::cin >> rate;
	std::cout << "\nFinally, what is the starting balance of your " << name << " account?";
	std::cin  >>amount;
	std::cout << std::endl;

	if(amount[0]=='$'){
		amount=amount.erase(0,1);
		amt=std::stod(amount);
	}
	amt = std::stod(amount);
 
	Account New(name, rate, amt);//Initializing the object.
 
	return (New);
}



// Test Functions; *** DO NOT ALTER ***
bool moneyGetPennies(int arg)
{
	Money money(arg / 100, arg % 100);
	return money.getPennies() == arg;
}

bool moneyIsNegative(bool expected)
{
	if (expected == true) {
		Money money1(-5, 55);
		Money money2(5, -55);
		Money money3(-5);
		Money money4(-5.55);
		return ( ( money1.isNegative() && 
					money2.isNegative() && 
					money3.isNegative() && 
					money4.isNegative() ) == expected );
	} else {
		Money money1(5, 55);
		Money money2(5);
		Money money3(5.55);
		Money money4;
		return ( !( !(money1.isNegative()) &&
					!(money2.isNegative()) && 
					!(money3.isNegative()) &&
					!(money4.isNegative()) ) == expected );
	}
}

void moneyPrint(double arg){
	Money money(arg);
	money.print();
}

bool moneyAdd()
{
	Money left(4, 64), right(5, 36);
	left.add(right);										
	return left.isEqual(10);
}
bool moneySubtractPos()
{
	Money money(20);
	money.subtract(10.11);
	return money.isEqual(9.89);
}

bool moneySubtractNeg()
{
	Money money(20);
	money.subtract(30.11);
	
	return money.isEqual(-10.11);
}


bool acctGetName()
{
	Account account;

	return (account.getName() == "Savings");
}

bool acctGetRate()
{
	Account account("Savings", 0.01, Money(5, 0));

	return (account.getRate() == 0.01);
}

void acctPrint(double arg)
{
	Account account("Savings", 0.015, arg);
	account.print();
}

bool acctGetBalance()
{
	Money money(50);
	Account account("Savings", 0.01, money);
	Money bal = account.getBalance();

	return (bal.isEqual(money));
}

bool acctSetName()
{
	std::string name = "ROTH IRA";
	Account account("Savings", 0.01, 5);

	account.setName(name);
	return (name == account.getName());
}

bool acctDep01()
{
	Money money(5.63);
	Account account;

	account.deposit(money);

	return ((account.getBalance()).isEqual(money));
}

bool acctDep02()
{
	Account account;

	account.deposit(5, 63);

	return ((account.getBalance()).isEqual(5.63));
}

bool acctDep03()
{
	Account account;

	account.deposit(20);

	return ((account.getBalance()).isEqual(20));
}

bool acctDep04()
{
	Account account;

	account.deposit(5.63);

	return ((account.getBalance()).isEqual(5.63));
}


void acctBadDep()
{
	Account account;
	account.deposit(-50);
}

bool acctWith01()
{
	Money money(25.44);
	Account account("Savings", 0.015, 50);

	Money w = account.withdraw(money);

	return (w.isEqual(money) && (account.getBalance()).isEqual(24.56));
}

bool acctWith02()
{
	Account account("Savings", 0.015, 50);

	Money w = account.withdraw(25, 44);

	return (w.isEqual(25.44) && (account.getBalance()).isEqual(24.56));
}

bool acctWith03()
{
	Account account("Savings", 0.015, 50);

	Money w = account.withdraw(24);

	return (w.isEqual(24) && (account.getBalance()).isEqual(26));
}

bool acctWith04()
{
	Account account("Savings", 0.015, 50);

	Money w = account.withdraw(25.44);

	return (w.isEqual(25.44) && (account.getBalance()).isEqual(24.56));
}

bool acctWithPart01()
{
	Account account("Checking", 0.015, 54.33);

	Money w = account.withdraw(60);

	return w.isEqual(60) && (account.getBalance()).isEqual(-5.67);
}

bool acctWithPart02()
{
	Account account("Checking", 0.015, 54.33);

	Money w = account.withdraw(300);

	return w.isEqual(104.33) && (account.getBalance()).isEqual(-50);
}

void acctBadWith()
{
	Account account;

	account.withdraw(-10);
}

bool acctAccrue()
{
	Account account("Roth", 0.07, 1000);
	
	account.accrue();

	return (account.getBalance()).isEqual(1070);
}

bool acctTransfer()
{
	Account savings;
	Account checking("Checking", 0.015, 2500);

	checking.transferTo(savings, 500);

	return (savings.getBalance()).isEqual(500) && (checking.getBalance()).isEqual(2000);
}



